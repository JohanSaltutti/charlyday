<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:27
 */

namespace ccd\views;
use ccd\models\Categorie;

class CategorieDetailView extends View
{
    private $idCategorie = -1;

    public function __construct($idCat)
    {


        if(Categorie::where(['id' => $idCat])->first() != null)
            $this->idCategorie = $idCat;
        else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('categorie'));
        }
    }

    public function render(){



        $header = $this->header();
        $head = $this->head();

        $items = $this->getItem($this->idCategorie);

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $html = "

<html>
            
      <head>
      
      $head
      <link rel='stylesheet' href='$link/assets/css/categorieDetail.css'>
       </head>
            
       <body>
           $header
           $items
           
        </body>
            
            
</html>
        
        ";
        echo $html;

    }


    public function getItem($idcategorie){
      $cat = Categorie::where(['id' => $idcategorie])->first();
      $items = $cat->items()->get();


      $res = "
        <section class='titre'>
            <div class='container'>
              <h1>$cat->nom</h1>
              <p>$cat->description</p>
            </div>
        </section>
      ";

      $res .= "<section class='items'>";

       foreach ($items as $item){
           $app = \Slim\Slim::getInstance();


           $urlItem = $app->urlFor('detailItem', ['i' => $item->id]);

           if($item->img != "")
                $urlImage = $app->request()->getUrl() . $app->request()->getRootUri() . "/assets/img/item/" . $item->img;
           else
               $urlImage = "https://i5.walmartimages.com/asr/f752abb3-1b49-4f99-b68a-7c4d77b45b40_1.39d6c524f6033c7c58bd073db1b99786.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF";


            $res .= "
                <a href='$urlItem' class='item'>
                  <img src='$urlImage' alt=''>
                  <p>$item->nom</p>
                </a>
            ";
        }

         $res .= '  </section>';

        return $res;
    }
}