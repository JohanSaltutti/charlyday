<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:23
 */

namespace ccd\views;


use ccd\models\Categorie;
use Slim\Slim;

class AdminListeCategorie extends View
{

    public function render()
    {

        $header = $this->header();
        $head = $this->head();

        $cat = $this->getCat();

        $app = Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $html = "
<html>
            
      <head>
      
      $head
      <link rel='stylesheet' href='$link/assets/css/adminListeCategorie.css'>
       </head>
            
       <body>
       
           $header
           ".parent::error()."
           
           $cat
        </body>
            
            
</html>
        
        ";

        echo $html;
    }

    private function getCat(){
        $cat = Categorie::all();
        $app = Slim::getInstance();

        $res = "<table>
                    <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                    </thead>
                    <tbody>";

        foreach ($cat as $c){


            $res .= "<tr> 
                        <td>$c->nom</td>
                        <td>$c->description</td>
                        <td>
                            <a href='".$app->urlFor('additem',['id' => $c->id])."'>Ajouter item</a>
                            <a href='".$app->urlFor('modcat', ['id' => $c->id])."' >Modifier</a>
                            <a href='".$app->urlFor('supcat', ['id' => $c->id])."'>Supprimer</a>
                        </td>
                     </tr>";
        }


        $res .= "</tbody></table>";

        return $res;
    }

}