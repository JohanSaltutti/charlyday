<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:36
 */

namespace ccd\views;
use ccd\models\Item;

class ItemView extends View
{

    private $idItem = -1;

    public function __construct($id){
        $this->idItem = $id;
    }

    public function render()
    {

        $header = $this->header();
        $head = $this->head();
        $item = $this->item();

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();


        $html = "
<html>
            
      <head>
      
      $head
      <link rel='stylesheet' href='$link/assets/css/item.css'>
       </head>
            
       <body>
       
           $header
           $item
        </body>
            
            
</html>
        
        ";

        echo $html;
    }

    public function item(){

        $item = Item::where(['id' => $this->idItem])->first();
        if($item != null){

            $app = \Slim\Slim::getInstance();
            $linkPlanning = $app->urlFor('planningItem', ['i' => $this->idItem]);
            $urlReservation = $app->urlFor('reservationItem', ['i' => $item->id]);
            $urlImg = $urlImage = $app->request()->getUrl() . $app->request()->getRootUri() . "/assets/img/item/" . $item->img;


            $s = "
                   
                <section class='item'>
                  <div class='container'>
                    <h1>$item->nom</h1>
                    <p>$item->description</p>
                    <img src='$urlImg'/>
                
                    <a class='button' href='$linkPlanning'>Lien vers planning</a>
                    <a class='button' href='$urlReservation'>Reserver</a>
                
                    </div>
                </section>
                

";
            return $s;

        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('home'));
        }



    }
}