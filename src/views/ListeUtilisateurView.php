<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:32
 */

namespace ccd\views;


use ccd\models\User;

class ListeUtilisateurView extends View
{

    public function render(){
        $head = parent::head();
        $header = parent::header();

        $use = $this->getListe();

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $html = "
            <html lang='fr'>
                <head>   
                    $head
                    <link rel='stylesheet' href='$link/assets/css/users.css'>
                </head>
                <body>
                
                    $header
                    $use
                    
                 
                </body>
            </html>
        ";

        echo $html;
    }



    private function getListe(){
        $users = User::all();
        $app = \Slim\Slim::getInstance();
        $res = "     
            <section id='users'>
                <div class='container'>
                    <h1>Liste des utilisateurs</h1>
                    
                    <ul>
        ";



        foreach ($users as $u){


            $urlImg = $urlImage = $app->request()->getUrl() . $app->request()->getRootUri() . "/assets/img/user/" . $u->img;
            $res .= "<li>
                        <p>$u->nom $u->prenom</p>
                        <img src='$urlImg'/>
                    </li>";

        }

        $res .= '</ul>
            </div>
            </section>
            ';

        return $res;
    }
}