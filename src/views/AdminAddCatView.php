<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:23
 */

namespace ccd\views;


use Slim\Slim;

class AdminAddCatView extends View
{

    public function render()
    {

        $header = $this->header();
        $head = $this->head();

        $app = Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $html = "
<html>
            
      <head>
      
      $head
      <link rel='stylesheet' href='$link/assets/css/adminCatView.css'>
      
       </head>
            
       <body>
       
           $header
           ".parent::error()."
           
           <div class='box'>
           
               <form action='' method='post'>
                
                    <input type='text' id='nom' name='nom' placeholder='Nom de la catégorie'>
                    <input type='text' id='description' name='description' placeholder='Description de la catégorie'>
                    
                    <input type='submit' value='Ajouter'>
               
               
                </form>
</div>
        </body>
            
            
</html>
        
        ";

        echo $html;
    }

}