<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:47
 */

namespace ccd\views;
use ccd\models\Item;
use ccd\models\User;

class PlanningView extends View
{

    private $idItem = -1;

    public function __construct($id)
    {
        $this->idItem = $id;
    }

    public function render(){
        $head = parent::head();
        $header = parent::header();

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $planning = $this->planning();

        $html = "
            <html lang='fr'>
                <head>   
                    $head
                    <link rel='stylesheet' href='$link/assets/css/planning.css'>
                </head>
                <body>
                
                    $header
                    
                    $planning
                 
                </body>
            </html>
        ";

        echo $html;
    }

    public function planning(){

        $item = Item::where(['id' => $this->idItem])->first();

        if($item != null){
           $reservations = $item->reservations()->get();
           //$reservationsConfirme = $reservations->where(['etat' => 'confirme'])->get();
            $s = "<section class='planning'>
                <h1>Les reservations de l'item $item->nom sont : </h1>";

           if(count($reservations) != 0){
               foreach($reservations as $res){

                   $jourDebutEnInt = intval($res->time_debut / 100);
                   $heureDebut = $res->time_debut - 100*$jourDebutEnInt;

                   $jourFinEnInt = intval($res->time_fin / 100);
                   $heureFin = $res->time_fin - 100*$jourFinEnInt;

                   switch($jourDebutEnInt){
                       case 1 :
                           $jourDebutEnString = "Lundi";
                           break;
                       case 2 :
                           $jourDebutEnString = "Mardi";
                           break;
                       case 3 :
                           $jourDebutEnString = "Mercredi";
                           break;
                       case 4 :
                           $jourDebutEnString = "Jeudi";
                           break;
                       case 5 :
                           $jourDebutEnString = "Vendredi";
                           break;

                   }

                   switch($jourFinEnInt){
                       case 1 :
                           $jourFinEnString = "Lundi";
                           break;
                       case 2 :
                           $jourFinEnString = "Mardi";
                           break;
                       case 3 :
                           $jourFinEnString = "Mercredi";
                           break;
                       case 4 :
                           $jourFinEnString = "Jeudi";
                           break;
                       case 5 :
                           $jourFinEnString = "Vendredi";
                           break;
                   }


                   $user = User::where('id', '=', $res->id_user)->first();
                   $s .= "
            
                    <div class='reservation'>
                        <p>Reservé par $user->prenom  $user->nom de :</p>
                        
                        <div class='left'>
                            <p>$jourDebutEnString $heureDebut h</p>
                        </div>
                        
                        <p class='a'>à</p>
                        
                        <div class='right'>
                            <p>$jourFinEnString $heureFin h</p>
                        </div> 
                        
                        <a href='#'>Detail</a>
                    </div>
            
            
            
                ";
               }
           }else{
               $s .= "<p style='margin-top:60px'>L'item n'est pas encore reservé !</p>";
           }


            return $s;

        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('home'));
        }

    }

}