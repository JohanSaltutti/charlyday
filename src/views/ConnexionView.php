<?php

namespace ccd\views;

class ConnexionView extends View
{
    //methode d'affichage
    public function render(){

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $head = parent::head();
        $header = parent::header();

        $connexion = $this->connexion();

    $html = "
<html lang='fr'>
    <head>
        $head
        
        <link rel='stylesheet' href='$link/assets/css/connexion.css'>
    </head>
    <body>
    
        $header
    
    
        $connexion
        
    
    
    </body>
</html>";

    echo $html;
    }

    //bloc de connexion
    private function connexion(){
        return "
        <section id='connection'>

        <div class='box'>
    <h2>Connexion <i class='fa fa-gift' aria-hidden='true'></i></h2>
    <form action='' method='post'>
      <div>
        <input type='text' autocomplete='off' name='mail' required id='mail' autofocus='autofocus'>
        <label for='mail'>Adresse mail *</label>
      </div>

      <div>
        <input type='password' autocomplete='off' name='password' required id='pwd'>
        <label for='pwd'>Mot de passe *</label>
      </div>

      <input type='submit' value='Se connecter'>
        <p>Tu n'as pas de compte ? <a href='inscription'>Inscris toi !</a></p>
    </form>
  </div>

</section>
";
    }
}