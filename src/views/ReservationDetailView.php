<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:36
 */

namespace ccd\views;
use ccd\models\Reservation;

class ReservationDetailView extends View
{

    private $idReservation = -1;

    public function __construct($id){
        $this->idReservation = $id;
    }

    public function render()
    {

        $header = $this->header();
        $head = $this->head();
        $reservation = $this->reservation();

        $html = "
<html>
            
      <head>
      
      $head
      
       </head>
            
       <body>
       
           $header
           $reservation
        </body>
            
            
</html>
        
        ";

        echo $html;
    }

    public function reservation(){

        $reservation = Reservation::where(['id' => $this->idReservation])->first();

        if($reservation != null){

             $jourDebutEnInt = intval($reservation->time_debut / 100);
               $heureDebut = $reservation->time_debut - 100*$jourDebutEnInt;

               $jourFinEnInt = intval($reservation->time_fin / 100);
               $heureFin = $reservation->time_fin - 100*$jourFinEnInt;

               switch($jourDebutEnInt){
                   case 1 :
                       $jourDebutEnString = "Lundi";
                       break;
                   case 2 :
                       $jourDebutEnString = "Mardi";
                       break;
                   case 3 :
                       $jourDebutEnString = "Mercredi";
                       break;
                   case 4 :
                       $jourDebutEnString = "Jeudi";
                       break;
                   case 5 :
                       $jourDebutEnString = "Vendredi";
                       break;

               }

               switch($jourFinEnInt){
                   case 1 :
                       $jourFinEnString = "Lundi";
                       break;
                   case 2 :
                       $jourFinEnString = "Mardi";
                       break;
                   case 3 :
                       $jourFinEnString = "Mercredi";
                       break;
                   case 4 :
                       $jourFinEnString = "Jeudi";
                       break;
                   case 5 :
                       $jourFinEnString = "Vendredi";
                       break;
               }


            $s = "
                <p></p>
                <p>reserve par $user->nom</p>
                <p>reserve le $jourDebutEnString a $heureDebut heure </p>
                <p>jusque le $jourFinEnString a $heureFin heure </p>
                <p>Voici l'etat de la reservation: $reservation->etat</p>";
            return $s;

        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('home'));
        }



       




        



    }
}