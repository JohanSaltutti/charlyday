<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:11
 */

namespace ccd\views;

use Slim\Slim;

class HomeView extends View
{

    public function render(){

        $header = $this->header();
        $head = $this->head();
        $main = $this->main();

        $html = "
<html>
             
      <head>
      
      $head
      
       </head>
            
       <body>
       
           $header
           ".parent::error()."
            $main
        </body>
            
            
</html>
        
        ";
        echo $html;

    }

//premiere section
    private function main(){

        $app = \Slim\Slim::getInstance();


        return " <section id='main'>
            <div class='main-content'>
              <h1>Garage <span class='red'>Planning</span>Manager</h1>
            
              <h4>{ Berard • Capone • Saltutti • Sommer • Zink }</h4>

              <a class='btn-main' href='".$app->urlFor('categorie')."'>Liste des<br/>catégories</a>
              <a class='btn-main' href='".$app->urlFor('users')."'>Liste des utilisateurs</a>
            </div>
          </div>
        </section>";
    }
}