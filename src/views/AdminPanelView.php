<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:23
 */

namespace ccd\views;


use ccd\models\User;
use Slim\Slim;

class AdminPanelView extends View
{

    public function render()
    {

        $header = $this->header();
        $head = $this->head();

        $listU = $this->changeUser();

        $app = Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $html = "
<html>
            
      <head>
      <link rel='stylesheet' href='$link/assets/css/adminPanel.css'>
      $head
      
       </head>
            
       <body>
       
           $header
           ".parent::error()."
           <h1>Administration</h1>
           <div class='bts'>
           
                <ul>
                    <li><a style='color: black;' href='" . $app->urlFor('addCat') . "'>Ajouter une catégorie</a></li>
                    <li><a style='color: black;' href='" . $app->urlFor('listCat') . "'>Lister les catégories</a></li>
                    <li><a style='color: black;' href='" . $app->urlFor('adminReservation') . "'>Lister les réservations</a></li>    
                </ul>
            <form action='' method='post'>
            
                $listU
                
                <input type='submit' value='Mettre en administrateur'>
            </form>
            </div>
            
            
        </body>
            
            
</html>
        
        ";

    echo $html;
    }

    private function changeUser(){


        $user = User::all();

        $res = "<select name='users'>";

        foreach ($user as $u){
            $res .="<option value='$u->id'>$u->nom $u->prenom</option>";
        }

        $res .= "</option>";

        return $res;

    }

}