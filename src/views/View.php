<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:12
 */

namespace ccd\views;
use ccd\models\User;

class View
{
    
    public function head(){

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        return "
                  <meta charset='UTF-8'>
                  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                  <title>CharlyDay</title>
                  
                  <link rel='stylesheet' href='$link/assets/css/grillade.css'>
                  <link rel='stylesheet' href='$link/assets/css/global.css'>
                  <link rel='stylesheet' href='$link/assets/css/header.css'>
                  <link rel='stylesheet' href='$link/assets/css/index.css'>
                  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
                ";
    }

    public function error(){

        if(isset($_SESSION['slim.flash']['error'])){
            return "<div class='errors'><p class='p_error'>".$_SESSION['slim.flash']['error']."</p></div>";
        }

        if(isset($_SESSION['slim.flash']['success'])){
            return "<div class='success'><p class='p_success'>".$_SESSION['slim.flash']['success']."</p></div>";
        }

        return "";
    }

    //renvoie le menu
    public function header(){
        $app = \Slim\Slim::getInstance();

        $nav = "";

        if(isset($_SESSION['compte']['iduser'])){
            $queryUser = User::where('id', '=', $_SESSION['compte']['iduser'])->first();
            $nav = "<div id='user'>
                        <div id='username'>
                          <p>".$queryUser->prenom." <i class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i></i></p>
                        </div>
                
                
                          <div id='sous-menu'>
                            <a href='".$app->urlFor("profil")."'>Mon profil</a>";
            if($_SESSION['compte']['type'] == 1) {  
                $nav .= "<a href='".$app->urlFor("admin")."'>Administration</a>";
            }
                $nav .= "   <a href='".$app->urlFor("mesReservations")."'>Réservations</a>
                            <a href='".$app->urlFor("deconnexion")."'>Deconnexion</a>
                          </div>
                        </div>";

        }else{
            $nav = "<a class='menu-item btn_inscription' href='".$app->urlFor("inscription")."'>Inscription</a>
                    <a class='menu-item btn_connexion' href='".$app->urlFor("login")."'>Connexion</a>";
        }

        return "

        <header>
          <div id='logo'>
            <p><a href='".$app->urlFor('home')."'>Garage Planning Manager</a></p>
            <div class='header-img'></div>
          </div>
    
        
          <nav>
          
          
          $nav
    
    
    
          </nav>
          <!--  -->
        </header>";
    }

}