<?php

namespace ccd\views;


use ccd\models\User;
use Slim\Slim;

class ProfilView extends View
{

    //methode d'affichage
    public function render(){

        $head = parent::head();
        $header = parent::header();

        $content = $this->content();

        $html = "
<html lang='fr'>
    <head>
        $head
        <link rel='stylesheet' href='assets/css/profil.css'>
        <script src=\"https://use.fontawesome.com/3006071a67.js\"></script>
    </head>
    <body>
    
        $header
        $content
    
    
    </body>
</html>";

        echo $html;

    }

    //affiche les informations de l'utilisateur
    private function content(){
        $user = User::where("id","=", $_SESSION['compte']['iduser'])->first();

        $app = Slim::getInstance();

        $img =  $app->request()->getUrl() . $app->request()->getRootUri() . "/assets/img/user/" . $user->img;
        return "<section id='profil'>
            
            
            
            
            <div class='content grid-12'>
            
             <div class='image col-3'>
                <img src='$img' alt=''>
    
             </div>
             
             <div id='items' class='col-9'> 
                  
                  <form class='item grid-12' action='' method='post'>
                       <label class='col-4' for='nom'>Votre nom</label>
                        <input type='text' name='nom' id='nom' class='col-7' value='".$user->nom."'>
                        <button type='submit' class='col-1 btn_save'><i class='fa fa-check fa-3x'></i></button>
                  </form>
                  
                  <form class='item grid-12' action='' method='post'>
                       <label class='col-4' for='prenom'>Votre prenom</label>
                        <input type='text' name='prenom' id='prenom' class='col-7' value='".$user->prenom."'>
                        <button type='submit' class='col-1 btn_save'><i class='fa fa-check fa-3x'></i></button>
                  </form>
                  
                  <form class='item grid-12' action='' method='post'>
                       <label class='col-4' for='email'>Votre email</label>
                        <input type='email' name='email' id='email' class='col-7' value='".$user->email."'>
                        <button type='submit' class='col-1 btn_save'><i class='fa fa-check fa-3x'></i></button>
                  </form>
                  
                  <form class='item grid-12' action='' method='post'>
                       <label class='col-4' for='password'>Nouveau mot de passe</label>
                        <input type='password' name='password' id='password' class='col-7' placeholder='Votre nouveau mot de passe'>
                        <button type='submit' class='col-1 btn_save'><i class='fa fa-check fa-3x'></i></button>
                  </form>
                  
                  <form  action='' method='post'>
                  <input type='hidden' name='delete'>
                        <button type='submit' class='col-1 btn_delete'><i class='fa fa-trash fa-2x'></i>Supprimer son compte</button>
                  </form>
                  

          
             </div>
             
          </div>
        </section>";
    }

}