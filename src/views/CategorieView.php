<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:22
 */

namespace ccd\views;

use ccd\models\Categorie;


class CategorieView extends View
{

    public function render(){

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $header = $this->header();
        $head = $this->head();
        $categories = $this->getCategorie();

        $html = "
        <html>
            
              <head>
              
              $head
              <link rel='stylesheet' href='$link/assets/css/categorie.css'>
              
               </head>
                    
               <body>
               
                   $header
                   
                     <section class='categories'>
                         $categories
                     </section>
                     
                </body>
                     
        </html>
        
        ";
        echo $html;
    }

    public function getCategorie(){
        $s = "";
        $cat = Categorie::all();
        $app = \Slim\Slim::getInstance();

        foreach($cat as $c){
            $img = $c->items()->first()->img;
            if(strlen($img) == 0)
                $urlImg = "http://www.51allout.co.uk/wp-content/uploads/2012/02/Image-not-found.gif";
            else
                $urlImg = $app->request()->getUrl() . $app->request()->getRootUri() . "/assets/img/item/" . $img;

            $link = \Slim\Slim::getInstance()->urlFor('detailCategorie', ['i' => $c->id]);

            $s .= "
            
                <article class='card'>

                      <header class='card__thumb'>
                        <img src='$urlImg' alt=''>
                      </header>
                
                      <div class='card__body'>
                        <div class='card__body__category'><a href='#'>$c->nom</a></div>
                        <h2 class='card__body__title'><a href='#'>$c->nom</a></h2>
                
                        <p class='card__body__description'>$c->description</p>
                        <a class='card__body__detail' href='$link'>Voir la catégorie</a>
                      </div>

           </article>
            
            ";
        }

        return $s;
    }
}