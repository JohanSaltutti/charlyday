<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:30
 */

namespace ccd\views;
use ccd\models\User;

class ListeCompteView extends View
{

    public function render(){
        $head = parent::head();
        $header = parent::header();

        $compte = $this->listeCompte();

        $html = "
            <html lang='fr'>
                <head>   
                    $head
                    <link rel='stylesheet' href=''>
                </head>
                <body>
                
                    $header
                    $compte
                 
                </body>
            </html>
        ";

        echo $html;
    }


    public function listeCompte(){

        $app = \Slim\Slim::getInstance();

        $queryUsers = User::all();

        $res = '<ul>';

        foreach ($queryUsers as $user){


            $res .= "<li><a style='color: black;' href='". $app->urlFor("connexion", ['i' => $user->id]) ."'>$user->prenom</a></li>";
        }

        $res .= '</ul>';

        return $res;
    }

}