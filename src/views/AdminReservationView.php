<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:26
 */

namespace ccd\views;


use ccd\models\Reservation;
use ccd\models\User;
use ccd\models\Item;
use Slim\Slim;

class AdminReservationView extends View
{

    public function render(){


        $header = $this->header();
        $head = $this->head();
        $etatAttente = $this->etatReservationAttente();
        $etatAll = $this->etatReservationAll();

        $app = Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $html = "
<html>
            
      <head>
      
      $head      
      <link rel='stylesheet' href='$link/assets/css/adminReservation.css'>

      
       </head>
            
       <body>
       
           $header
        <div class='div1'>
            
            <h1>Reservation à accepter</h1>
            $etatAttente

            <h1>Historique des réservations</h1>
            $etatAll
        </div>
        </body>
            
            
</html>
        
        ";
        echo $html;

    }


    private function etatReservationAttente(){
        $app = Slim::getInstance();


        $reservation = Reservation::where('etat','=', "reserve")->get();


        $res = "<table class='tab1'>
                    <thead>
                            <tr>
                                <th>Jour depart</th>
                                <th>Heure depart</th>
                                <th>Jour fin</th>
                                <th>Heure fin</th>
                                <th>Nom de l'utilisateur</th>    
                                <th>Nom de l'item</th>    
                                <th>Etat</th>    
                                <th>Action</th>    
                            </tr>
                    </thead>
                    <tbody>";

        foreach ($reservation as $r){


            $nomItem =  Item::where('id', $r->id_item)->first()->nom;

            $idUser = $r->id_user;
            $user = User::where('id', '=', $idUser)->first();
            $name = $user->nom . $user->prenom;

            $urlA = $app->urlFor('adminReservation');

            $jourDebutEnInt = intval($r->time_debut / 100);
            $heureDebut = $r->time_debut - 100*$jourDebutEnInt;

            $jourFinEnInt = intval($r->time_fin / 100);
            $heureFin = $r->time_fin - 100*$jourFinEnInt;

            switch($jourDebutEnInt){
                case 1 :
                    $jourDebutEnString = "Lundi";
                    break;
                case 2 :
                    $jourDebutEnString = "Mardi";
                    break;
                case 3 :
                    $jourDebutEnString = "Mercredi";
                    break;
                case 4 :
                    $jourDebutEnString = "Jeudi";
                    break;
                case 5 :
                    $jourDebutEnString = "Vendredi";
                    break;

            }

            switch($jourFinEnInt){
                case 1 :
                    $jourFinEnString = "Lundi";
                    break;
                case 2 :
                    $jourFinEnString = "Mardi";
                    break;
                case 3 :
                    $jourFinEnString = "Mercredi";
                    break;
                case 4 :
                    $jourFinEnString = "Jeudi";
                    break;
                case 5 :
                    $jourFinEnString = "Vendredi";
                    break;
            }

            $res .= "<tr> 
                        <td>$jourDebutEnString</td>
                        <td>$heureDebut</td>
                        <td>$jourFinEnString</td>
                        <td>$heureFin</td>
                        <td>".$name."</td>
                        <td>".$nomItem."</td>
                        <td>$r->etat</td>
                        <td>
                            <a href='$urlA/accept/$r->id'>Accepter</a>
                            <a href='$urlA/refuse/$r->id' >Annuler</a>
                        </td>
                     </tr>";
        }


        $res .= "</tbody></table>";
        return $res;

    }


    private function etatReservationAll(){

        $reservation = Reservation::where('etat','!=', "reserve")->get();

        $res = "<table>
                    <thead>
                            <tr>
                            <th>Jour depart</th>
                                <th>Heure depart</th>
                                <th>Jour fin</th>
                                <th>Heure fin</th>
                                <th>Nom de l'utilisateur</th>    
                                <th>Nom de l'item</th>    
                                <th>Etat</th>    
                            </tr>
                    </thead>
                    <tbody>";

        foreach ($reservation as $r){

            $idUser = $r->id_user;
            $user = User::where('id', '=', $idUser)->first();
            $name = $user->nom . $user->prenom;
            $nomItem =  Item::where('id', $r->id_item)->first()->nom;

            $jourDebutEnInt = intval($r->time_debut / 100);
            $heureDebut = $r->time_debut - 100*$jourDebutEnInt;

            $jourFinEnInt = intval($r->time_fin / 100);
            $heureFin = $r->time_fin - 100*$jourFinEnInt;

            switch($jourDebutEnInt){
                case 1 :
                    $jourDebutEnString = "Lundi";
                    break;
                case 2 :
                    $jourDebutEnString = "Mardi";
                    break;
                case 3 :
                    $jourDebutEnString = "Mercredi";
                    break;
                case 4 :
                    $jourDebutEnString = "Jeudi";
                    break;
                case 5 :
                    $jourDebutEnString = "Vendredi";
                    break;

            }

            switch($jourFinEnInt){
                case 1 :
                    $jourFinEnString = "Lundi";
                    break;
                case 2 :
                    $jourFinEnString = "Mardi";
                    break;
                case 3 :
                    $jourFinEnString = "Mercredi";
                    break;
                case 4 :
                    $jourFinEnString = "Jeudi";
                    break;
                case 5 :
                    $jourFinEnString = "Vendredi";
                    break;
            }


            $res .= "<tr> 
                        <td>$jourDebutEnString</td>
                        <td>$heureDebut</td>
                        <td>$jourFinEnString</td>
                        <td>$heureFin</td>
                        <td>".$name."</td>
                        <td>".$nomItem."</td>
                        <td>$r->etat</td>
                     </tr>";
        }


        $res .= "</tbody></table>";
        return $res;

    }


}