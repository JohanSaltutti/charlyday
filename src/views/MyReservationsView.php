<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 19:32
 */

namespace ccd\views;


use ccd\models\Reservation;
use ccd\models\Item;
use Slim\Slim;

class MyReservationsView extends View
{

    public function render(){

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();
        $reservations = $this->myRes();

        $head = parent::head();
        $header = parent::header();


        $app = Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();


        $html = "
<html lang='fr'>
    <head>
        $head
        <link rel='stylesheet' href='$link/assets/css/myres.css'>

    </head>
    <body>
    
        $header
        
        <h1>Mes réservations</h1>
        <div class='all'>
        
        $reservations
</div>
    
        
    
    
    </body>
</html>";

        echo $html;
    }


    public function myRes(){
        $res = Reservation::where('id_user','=',$_SESSION['compte']['iduser'])->get();

        $retur = '<ul>';

        foreach ($res as $r){
            $n =  Item::where('id', $r->id_item)->first()->nom;
            $retur .= "<li><a class='a' href=''>$n |  $r->etat</a></li>";
        }

        $retur .= '</ul>';

        return $retur;
    }
}