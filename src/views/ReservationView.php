<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 08/02/2018
 * Time: 10:39
 */

namespace ccd\views;
use ccd\models\Item;

class reservationView extends View
{

    private $item ;

    public function __construct($id){

        $item = Item::where(['id' => $id])->first();

        if($item != null)
            $this->item = $item;

        else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('home'));
        }
    }


    public function render(){
        $head = parent::head();
        $header = parent::header();


        $form_reservation = $this->form_reservation();

        $app = \Slim\Slim::getInstance();
        $link = $app->request()->getUrl() . $app->request()->getRootUri();

        $nomItem = $this->item->nom;

        $html = "
            <html lang='fr'>
                <head>   
                    $head
                    <link rel='stylesheet' href='$link/assets/css/reservation.css'>
                </head>
                <body>
                
                    $header
                    ".parent::error()."
                    <h1>Reservation de l'item $nomItem</h1>
                    $form_reservation
                 
                </body>
            </html>
        ";

        echo $html;
    }

    public function form_reservation(){

            $s = "
            
            <form method='post' action=''>

            <div class='form_element'>
              <label for='jDepart'>Jour de debut</label><!--
              --><select id='jDepart' name='jDepart'>
                  <option value='1'>Lundi</option>
                  <option value='2'>Mardi</option>
                  <option value='3'>Mercredi</option>
                  <option value='4'>Jeudi</option>
                  <option value='5'>Vendredi</option>
                </select>
            </div>
            
            
            <div class='form_element'>
              <label for='hDepart'>Heure depart</label><!--
              --><select id='hDepart' name='hDepart'>
                <option value='8'>8h00</option>
                <option value='10'>10h00</option>
                <option value='12'>12h00</option>
                <option value='14'>14h00</option>
                <option value='16'>16h00</option>
              </select>
            </div>
            
            
            <div class='form_element'>
              <label for='jFin'>Jour de fin</label><!--
              --><select id='jFin' name='jFin'>
                  <option value='1'>Lundi</option>
                  <option value='2'>Mardi</option>
                  <option value='3'>Mercredi</option>
                  <option value='4'>Jeudi</option>
                  <option value='5'>Vendredi</option>
                </select>
            </div>
            
            <div class='form_element'>
                <label for='hFin'>Heure fin</label><!--
                --><select id='hFin' name='hFin'>
                  <option value='10'>10h00</option>
                  <option value='12'>12h00</option>
                  <option value='16'>16h00</option>
                  <option value='18'>18h00</option>
                </select>
            </div>
            
            
            <input type='submit' value='Valider'>
            </form>
                
";
            return $s;
    }

}