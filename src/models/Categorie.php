<?php

namespace ccd\models;

class Categorie extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'categorie';
    protected $primaryKey ='id';
    public $timestamps = false;

    //Il faut mettre id !
    public function items(){

        return $this->hasMany('ccd\models\Item' , 'id_categorie');

    }
}
