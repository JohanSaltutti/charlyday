<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 11:00
 */

namespace ccd\models;


class Reservation extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'reservation';
    protected $primaryKey ='id';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('ccd\models\User','id');
    }

    public function item(){
        return $this->belongsTo('ccd\models\Item','id');
    }

}

