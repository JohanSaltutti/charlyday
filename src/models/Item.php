<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 11:01
 */

namespace ccd\models;


class Item extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'item';
    protected $primaryKey ='id';
    public $timestamps = false;


    public function categorie(){
        return $this->belongsTo('ccd\models\Categorie', 'id_categorie');
    }

    public function reservations(){
        return $this->hasMany('ccd\models\Reservation', 'id_item');
    }

}


