<?php

$app = \Slim\Slim::getInstance();

//on verifie qu'un formulaire a ete poste
if (isset($_POST)){

    //cas où le formulaire est pour le changement de mot de passe
    if(isset($_POST['password'])){
        $password = password_hash(filter_var($_POST['password'],FILTER_SANITIZE_STRING), PASSWORD_DEFAULT,['cost' => 12]);
        \ccd\models\User::where('id',"=",$_SESSION['compte']['iduser'])->update(['mdp' => $password]);

    }

    //cas où le formulaire est pour le changement du nom de l'utilisateur
    if(isset($_POST['nom'])){
        $nom = filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
        \ccd\models\User::where('id',"=",$_SESSION['compte']['iduser'])->update(['nom' => $nom]);

    }

    //cas où le formulaire est pour le changement du prenom de l'utilisateur
    if(isset($_POST['prenom'])){
        $prenom =filter_var($_POST['prenom'],FILTER_SANITIZE_STRING);
        \ccd\models\User::where('id',"=",$_SESSION['compte']['iduser'])->update(['prenom' => $prenom]);

    }

    //cas où le formulaire est pour le changement de l'adresse email de l'utilisateur
    if(isset($_POST['email'])){
        $email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
        if($email){

            //verification que la nouvelle adresse email ne soit pas deja utilise
            $tmp = \ccd\models\User::where('email','=',$email);
            if($tmp==null){

                \ccd\models\User::where('id',"=",$_SESSION['compte']['iduser'])->update(['email' => $email]);
            }else{
                echo "vous en pouvez pas mettre cette adresse email";
                die();
            }
        }else{
            $app->flash('error', 'Adresse email non valide');
            $app->redirect('profil');
        }
    }

    //cas où le formulaire est pour la suppression d'un compte
    if(isset($_POST['delete'])){
        $user = \ccd\models\User::where('id',"=",$_SESSION['compte']['iduser'])->first();
        $user->delete();
        session_destroy();
        $app->flash('success', 'Votre compte a bien été supprimé');
        $app->redirect($app->urlFor('home'));

    }



}