<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:19
 */

namespace ccd\controllers;


use ccd\views\MyReservationsView;
use ccd\views\ReservationView;
use ccd\views\ProfilView;
use ccd\models\Item;
use ccd\models\Reservation;
use Slim\Slim;

class MembreControlleur
{


    public function getFormulaireReservationItem($id){
        $app = Slim::getInstance();
        if($i = Item::where('id',$id)){

            $v = new ReservationView($id);
            $v->render();
        }else{
            $app->redirect($app->urlFor('home'));
        }
    }

    public function mesReservation()
    {
        $v = new MyReservationsView();
        $v->render();
    }
    public function reserverItem($i)
    {

        $app = \Slim\Slim::getInstance();
        $item = Item::where(['id' => $i])->first();

        if($item != null){

            //1:lundi ... 5:vendredi
            $jourDepart = filter_var($_POST['jDepart'],FILTER_VALIDATE_INT);
            $heureDepart = filter_var($_POST['hDepart'],FILTER_VALIDATE_INT);

            $jourFin = filter_var($_POST['jFin'],FILTER_VALIDATE_INT);
            $heureFin = filter_var($_POST['hFin'],FILTER_VALIDATE_INT);

            $time_debut = $jourDepart * 100 + $heureDepart;
            $time_fin = $jourFin * 100 + $heureFin;

            if($time_debut < $time_fin){
                $cas1 = count($item->reservations()->where('time_debut', '<=', $time_debut)->where('time_fin', '>', $time_debut)->get());
                $cas2 = count($item->reservations()->where('time_debut', '<=', $time_fin)->where('time_fin', '>=', $time_fin)->get());
                $cas3 = count($item->reservations()->where('time_debut', '>=', $time_debut)->where('time_fin', '<=', $time_fin)->get());

                if($cas1 + $cas2 + $cas3 === 0){
                    $res = new Reservation();
                    $res->time_debut = $time_debut;
                    $res->time_fin = $time_fin;
                    $res->id_user = $_SESSION['compte']['iduser'];
                    $res->id_item = $i;
                    $res->note = -1;
                    $res->etat = "reserve";
                    $res->save();

                    $idCat = $item->categorie()->first()->id;
                    $app->redirect($app->urlFor('detailCategorie', ['i' => $idCat]));

                }else{

                    $app->flash('error', "L'item est déjà réservé pendant cette période");
                    $app->redirect($app->urlFor('reservationItem',['i' => $i]));
                
                }
            }else{

                $app->flash('error', "Les dates ne sont pas dans l'ordre");
                $app->redirect($app->urlFor('reservationItem',['i' => $i]));
            }

        }else{
            $app->redirect($app->urlFor('home'));
        }
    }



    //deconnexion
    public function getDeconnexion(){
        include 'actions/disconnect.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect('home');
    }

    //page du profil
    public function getProfil(){
        $v = new ProfilView();
        $v->render();
    }

    public function postProfil(){
        include 'actions/changeProfil.php';
        $this->getDeconnexion();
    }

}