<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:10
 */

namespace ccd\controllers ;

use ccd\views\HomeView;
use ccd\views\CategorieView;
use ccd\views\CategorieDetailView;
use ccd\views\ItemView;
use ccd\views\ListeCompteView;
use ccd\views\ListeUtilisateurView;
use ccd\views\PlanningView;
use ccd\views\ConnexionView;
use ccd\views\InscriptionView;
use ccd\models\User;


class VisiteurControlleur
{
    public function getHome(){
        $v = new HomeView();
        $v->render();
    }


    public function getCategorie(){
        $v = new CategorieView();
        $v->render();
    }

    public function getCategorieDetail($i){
        //if($i est un categorie)
        $v = new CategorieDetailView($i);
        $v->render();
        //else
        /*
         * $app = \Slim\Slim::getInstance();
         * $app->redirect($app->urlFor('categorie'))
         *
         */
    }

    public function getItem($i){
        //if($i est un item)
        $v = new ItemView($i);
        $v->render();
    }

    public function listeCompte(){
        $v = new ListeCompteView();
        $v->render();
    }

    public function listeCompteLog($i) {
        $_SESSION['compte']['iduser'] = $i;
        $queryUser = User::where('id', '=', $i)->first();
        $_SESSION['compte']['type'] = $queryUser->type;
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor('home'));
    }

    public function listeUtilisateur(){
        $v = new ListeUtilisateurView();
        $v->render();
    }

    public function afficherPlanningItem($id){
        $v = new PlanningView($id);
        $v->render();
    }

    //page de connexion
    public function getConnexion(){
        $v = new ConnexionView();
        $v->render();
    }

    public function postConnexion(){
        include 'actions/connect.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect('dashboard');
    }

    //page d'inscription
    public function getInscription(){
        $v = new InscriptionView();
        $v->render();
    }

    public function postInscription(){
        include 'actions/addUser.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect('home');
    }
}