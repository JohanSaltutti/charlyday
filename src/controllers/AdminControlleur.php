<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 08/02/2018
 * Time: 10:20
 */


namespace ccd\controllers;

use ccd\models\categorie;
use ccd\models\item;
use ccd\models\Reservation;
use ccd\views\AdminAddCatView;
use ccd\views\AdminAddItemView;
use ccd\views\AdminModItemView;
use ccd\views\AdminListeCategorie;

use ccd\views\AdminModCatView;
use ccd\views\AdminPanelView;
use ccd\views\AdminReservationView;
use Slim\Slim;

class AdminControlleur
{


    public function index()
    {
        $v = new AdminPanelView();
        $v->render();
    }

    public function reservation()
    {
        $v = new AdminReservationView();
        $v->render();
    }

    public function getListeCatView()
    {
        $v = new AdminListeCategorie();
        $v->render();
    }

    public function accepterRes($id)
    {
        $app = Slim::getInstance();

        $reservation = Reservation::where('id', $id)->first();
        $reservation->etat = "confirme";
        $reservation->save();

        $app->redirect($app->urlFor('adminReservation'));

    }

    public function annuleRes($id)
    {
        $app = Slim::getInstance();

        $reservation = Reservation::where('id', $id)->first();
        $reservation->etat = "annule";
        $reservation->save();

        $app->redirect($app->urlFor('adminReservation'));

    }

    public function getFormAddCatView()
    {
        $v = new AdminAddCatView();
        $v->render();
    }

    public function getFormModItemView($id)
    {
        $v = new AdminModItemView($id);
        $v->render();
    }

    public function getFormAddItemView()
    {
        $v = new AdminAddItemView();
        $v->render();
    }

    public function getFormModCatView($id)
    {
        $v = new AdminModCatView($id);
        $v->render();
    }

    public function getFormSupCatView($id)
    {
        $this->supprimercategorie($id);
    }

    public function getFormSuppItemView($id)
    {
        $this->supprimerItem($id);
    }

    //methode qui permet de creer une liste 
    public function ajouterCategorie()
    {
        $app = Slim::getInstance();
        $c = new Categorie();

        //verifie s'il existe bien une description
        if ($_POST['nom'] != "") {
            //filtre la description
            if ($_POST['nom'] == filter_var($_POST['nom'], FILTER_SANITIZE_STRING)) {
                $c->nom = $_POST['nom'];

            }
        } else {
            $app->flash('error', "Veuillez compléter le champ utilisateur");
            $app->redirect($app->urlFor('addCat'));
        }

        if ($_POST['description'] != "") {
            //filtre la description
            if ($_POST['description'] == filter_var($_POST['description'], FILTER_SANITIZE_STRING)) {
                $c->description = $_POST['description'];
            }
        }

        $c->save();
        $app->redirect($app->urlFor('addCat'));


    }

    public function modifierCategorie($idcategorie)
    {
        $app = Slim::getInstance();
        // if ($_SESSION['compte']['etat']==1) {
        $categorie = categorie::where('id', '=', $idcategorie)->first();
        if ($categorie != null) {
            //verifie s'il existe bien un nom
            if ($_POST['nom'] != "") {
                //filtre la description
                if ($_POST['nom'] == filter_var($_POST['nom'], FILTER_SANITIZE_STRING)) {
                    $categorie->nom = $_POST['nom'];

                }
            }else {
                $app->flash('error', "Veuillez compléter tous les champs");
                $app->redirect($app->urlFor('modcat',['id' => $idcategorie]));
            }

            if ($_POST['description'] != "") {
                //filtre la description
                if ($_POST['description'] == filter_var($_POST['description'], FILTER_SANITIZE_STRING)) {
                    $categorie->description = $_POST['description'];
                }
            }else {
                $app->flash('error', "Veuillez compléter tous les champs");
                $app->redirect($app->urlFor('modcat',['id' => $idcategorie]));
            }
            $categorie->update();
            $app->flash('success', "La catégorie a bien été mise à jour");
            $app->redirect($app->urlFor('modcat',['id' => $idcategorie]));
            // }
        }else{
            $app->redirect($app->urlFor('home'));
        }
    }

    public function supprimerCategorie($idcategorie)
    {
        $app = Slim::getInstance();
        $items = Item::where('id_categorie', '=', $idcategorie)->get();
        // if ($_SESSION['compte']['etat']==1) {
        foreach ($items as $item) {
            $item->delete();
        }
        $c = categorie::where('id', '=', $idcategorie);
        $c->delete();
        $app->flash('success', "La catégorie a bien été supprimé");
        $app->redirect($app->urlFor('listCat'));
        // }
    }

    //methode qui permet de supprimer l'idem en fonction de son id mis en parametre
    public function supprimerItem($id)
    {
        $app = Slim::getInstance();
        $item = \ccd\models\Item::where('id', '=', $id);
        $item->delete();
        $app->flash('success', "L'item a bien été supprimé");
        $app->redirect($app->urlFor('listCat'));
    }

//methode qui permet de modifier l'item en fonction de son id mis en parametre 
    public function modifierItem($id)
    {
        $app = Slim::getInstance();
        $i = Item::where('id', '=', $id)->first();
        //verif que nom non null
        if ($_POST['nom'] != "") {
            //filtre le nom
            if ($_POST['nom'] == filter_var($_POST['nom'], FILTER_SANITIZE_STRING)) {
                $i->nom = $_POST['nom'];
                $i->save();
            }
        }else {
            $app->flash('error', "Veuillez compléter tous les champs");
            $app->redirect($app->urlFor('moditem',['id'=>$id]));
        }

        //verif que descritpion non null
        if ($_POST['description'] != "") {
            //filtre descritpion
            if ($_POST['description'] == filter_var($_POST['description'], FILTER_SANITIZE_STRING)) {
                $i->description = $_POST['description'];
                $i->save();
            }
        }else {
            $app->flash('error', "Veuillez compléter tous les champs");
            $app->redirect($app->urlFor('moditem',['id'=>$id]));
        }

        //verif que tarif non null
        if ($_POST['commentaire'] != "") {
            //filtre tarif
            if ($_POST['commentaire'] == filter_var($_POST['commentaire'], FILTER_SANITIZE_NUMBER_FLOAT)) {
                $i->tarif = $_POST['commentaire'];
                $i->save();
            }
        }else {
            $app->flash('error', "Veuillez compléter tous les champs");
            $app->redirect($app->urlFor('moditem',['id'=>$id]));
        }
        $app->flash('success', "L'item a bien été modifié");
        $app->redirect($app->urlFor('listCat'));

    }

    //methode qui permet de creer une liste 
    public function ajouterItem($id)
    {
        $app = Slim::getInstance();
        $c = new Item();

        //verifie s'il existe bien une description
        if ($_POST['nom'] != "") {
            //filtre la description
            if ($_POST['nom'] == filter_var($_POST['nom'], FILTER_SANITIZE_STRING)) {
                $c->nom = $_POST['nom'];

            }
        }else {
            $app->flash('error', "Veuillez compléter tous les champs");
            $app->redirect($app->urlFor('additem',['id'=>$id]));
        }

        if ($_POST['description'] != "") {
            //filtre la description
            if ($_POST['description'] == filter_var($_POST['description'], FILTER_SANITIZE_STRING)) {
                $c->description = $_POST['description'];
            }
        }else {
            $app->flash('error', "Veuillez compléter tous les champs");
            $app->redirect($app->urlFor('additem',['id'=>$id]));
        }


        if ($_POST['commentaire'] != "") {
            //filtre la description
            if ($_POST['commentaire'] == filter_var($_POST['commentaire'], FILTER_SANITIZE_STRING)) {
                $c->commentaire = $_POST['commentaire'];
            }
        }else {
            $app->flash('error', "Veuillez compléter tous les champs");
            $app->redirect($app->urlFor('additem',['id'=>$id]));
        }
        $c->id_categorie = $id;
        $c->img = "dazdadzdazzdazz.png";
        $c->save();

        $cat = $c->categorie->nom;

        $app->flash('success', "L'item a bien été ajouté dans la catégorie $cat");
        $app->redirect($app->urlFor('listCat'));

    }

}