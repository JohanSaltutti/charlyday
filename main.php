<?php
session_start();
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;


$db = new DB();

$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$app = new \Slim\Slim();


$app->get('/', function() use($app){
   $app->redirect($app->urlFor('home'));
});

$app->get('/home', function(){
    $c = new ccd\controllers\VisiteurControlleur();
    $c->getHome();
})->name('home');

//Affiche l'ensemble des categories
$app->get('/categorie', function(){
    $c = new ccd\controllers\VisiteurControlleur();
    $c->getCategorie();
})->name('categorie');


//Affiche les items d'une categorie
$app->get('/categorie/:i', function($i){
   $c = new ccd\controllers\VisiteurControlleur();
   $c->getCategorieDetail($i);
})->name('detailCategorie');

//Affiche un item
$app->get('/item/:i', function($i){
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->getItem($i);
})->name('detailItem');


//Affiche le planning d'un item
$app->get('/item/:i/planning', function($i){
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->afficherPlanningItem($i);
})->name('planningItem');

//Affiche le formulaire de reservation d'un item
$app->get('/item/:i/reservation', function($i) use ($app){
    if(isset($_SESSION['compte'])){
        $c = new \ccd\controllers\MembreControlleur();
        $c->getFormulaireReservationItem($i);
    }else{
        $app->redirect($app->urlFor('login')); // a changer vers connexion apres
    }

})->name('reservationItem');

//Envoi le formulaire de reservation
$app->post('/item/:i/reservation', function($i){
    $c = new \ccd\controllers\MembreControlleur();
    $c->reserverItem($i);
});



//affiche la liste des comptes  pour se connecter
$app->get('/compte', function(){
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->listeCompte();
})->name('compte');

//affiche les utilisateurs
$app->get('/connexion/:i', function($i){
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->listeCompteLog($i);
})->name('connexion');


//affiche les utilisateurs
$app->get('/users', function(){
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->listeUtilisateur();
})->name('users');

$co = function (){
    $app = \Slim\Slim::getInstance();
    if(!isset($_SESSION['compte'])){
        $app->redirect($app->urlFor('home'));
    }
};

$admin = function (){
    $app = \Slim\Slim::getInstance();


    if(!isset($_SESSION['compte'])){
        $app->redirect($app->urlFor('home'));
    }

    if($_SESSION['compte']['type'] !== 1){
        $app->redirect($app->urlFor('home'));
    }
};

$app->get('/mesReservation', $co, function(){
    $c = new \ccd\controllers\MembreControlleur();
    $c->mesReservation();
})->name('mesReservations');

$app->group('/admin',$admin, function () use ($app) {

    //Affiche le panel admin
    $app->get('/', function(){
        $c = new \ccd\controllers\AdminControlleur();
        $c->index();
    })->name('admin');

    $app->post('/', function() use ($app){
        $user = \ccd\models\User::where('id',$_POST['users'])->first();

        if($user !== null){

            if($user->type !== 1){

                $user->type =1;
                $user->save();
                $app->flash('success', "L'utilisateur choisi à bien été mis administrateur");
                $app->redirect($app->urlFor('admin')) ;
            }else{
                $app->flash('error', "L'utilisateur est déjà administrateur");
                $app->redirect($app->urlFor('admin')) ;
            }

        }else{
            $app->flash('error', "Utilisateur inconnu");
            $app->redirect($app->urlFor('admin')) ;
        }

    });



    //affiche les reservations en attente
    $app->get('/reservation', function(){
        $c = new \ccd\controllers\AdminControlleur();
        $c->reservation();
    })->name('adminReservation');

    $app->get('/reservation/accept/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->accepterRes($id);
    });

    $app->get('/reservation/refuse/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->annuleRes($id);
    });

    $app->get('/addCat', function(){
        $c = new \ccd\controllers\AdminControlleur();
        $c->getFormAddCatView();
    })->name('addCat');

    $app->post('/addCat', function(){
        $c = new \ccd\controllers\AdminControlleur();
        $c->ajouterCategorie();

    });

    $app->get('/listCat', function(){

        $c = new \ccd\controllers\AdminControlleur();
        $c->getListeCatView();
    })->name('listCat');

    $app->get('/modCat/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->getFormModCatView($id);
    })->name('modcat');

    $app->post('/modCat/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->modifierCategorie($id);
    });

    $app->get('/supCat/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->getFormSupCatView($id);
    })->name('supcat');

    $app->get('/addItem/:id', function(){
        $c = new \ccd\controllers\AdminControlleur();
        $c->getFormAddItemView();
    })->name('additem');

    $app->post('/addItem/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->ajouterItem($id);
    });


    $app->get('/admin/modItem/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->getFormModItemView($id);
    })->name('moditem');

    $app->post('/admin/modItem/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->modifierItem($id);
    });

    $app->get('/admin/suppItem/:id', function($id){
        $c = new \ccd\controllers\AdminControlleur();
        $c->getFormSuppItemView($id);
    })->name('suppitem');

});

//deconnexion
$app->get('/deconnexion',function (){
    $c = new \ccd\controllers\MembreControlleur();
    $c->getDeconnexion();
})->name('deconnexion');


//connexion
$app->get('/login',function () use ($app){
    if(isset($_SESSION['profile'])){
        $app->redirect('dashboard');
    }
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->getConnexion();
})->name('login');



$app->post('/login',function (){
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->postConnexion();
});



//inscription
$app->get('/inscription',function () use ($app){
    if(isset($_SESSION['compte'])){
        $app->redirect('home');
    }
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->getInscription();
})->name('inscription');

$app->post('/inscription',function (){
    $c = new \ccd\controllers\VisiteurControlleur();
    $c->postInscription();
});


$app->notFound(function() use($app){
 $app->redirect($app->urlFor('home'));
});


//profil
$app->get('/profil', function () use ($app){
    if(isset($_SESSION['compte'])){
        $c = new \ccd\controllers\MembreControlleur();
        $c->getProfil();
    }else{
        $app->redirect('login');
    }

})->name('profil');

$app->post('/profil', function (){
    $c = new \ccd\controllers\MembreControlleur();
    $c->postProfil();
});


$app->run();