Crazy Charly Day 2018
=================
Le sujet cette année pour le département informatique est de développer un site internet permettant de réserver des véhicules et des ateliers de réparation, en vue de faire un "Garage solidaire"

### Le groupe est composé de :
    * Valentin BERARD
    * Florian CAPONE
    * Johan SALTUTTI
    * Matthieu SOMMER
    * Anthony ZINK

### Les fonctionalités réalisées durant cette journée sont :
###### Fonctionnalités normales :
    1. Afficher les catégories
    2. Afficher la liste des items d'une catégorie
    3. Afficher un item
    5. Accès sans authentification
    9. Afficher la liste des utilisateurs
    10. Compte administrateur
    11. Confirmation des réservations
    13. Afficher le planning textuel d'un item
    14. Afficher le détail d'une réservation
    18. Afficher le formulaire de réservation
    19. Enregistrer une fonctionnalité
    20. Confirmer une réservation
    21. Annuler une réservation

###### Fonctionnalités avancées :
    6. Créer un nouveau compte
    7. Accès avec authentification
    8. Modifier son compte
    12. Gestion du catalogue et des administrateurs
    24. (Notation d'un item)
		* Accessible seulement avec l'url : /item/1/noter

### Comment tester notre projet :
###### Pour se connecter, il faut utiliser le jeu de données suivant :    
    * Admin :
		* adresse mail : toto@toto.fr    
		* mot de passe : toto
    * Normal :
		* adresse mail : toto@toto.fr    
		* mot de passe : toto
###### Le lien permettant d'aller sur notre site est :    
[Lien vers le site](https://webetu.iutnc.univ-lorraine.fr/www/saltutti1u/ccd/home)
###### Si vous souhaitez tester la base, il ne faut pas oublier de changer le fichier de configuration situé :
    * ./charlyday/src/conf/conf.ini

### Informations complémentaires
Pour ce projet, nous n'avons pas utiliser de framework (autre que Slim, Eloquent, ...). C'est aussi Johan et Anthony qui ont fait l'intégration finale de A à Z.
  

###### Pour analyser et tester les items pour être sûr que cet item est totalement libre durant toute la période de réservation, nous avons fait différents tests comme :
    * test à cheval avant
    * test à cheval après
    * test à cheval entre les deux

L'intercroisement entre les périodes est donc vérifié.